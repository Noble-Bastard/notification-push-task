<?php

namespace App\Controller;

use App\NotificationsSender\Contracts\NotificationServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends AbstractController
{
    public function __construct(public NotificationServiceInterface $notificationsService)
    {
    }

    #[Route('/', name: 'notification_controller')]
    public function sendNotifications(Request $request): void
    {
        // Стоило бы добавить валидатор, но не успеваю(
        $this->notificationsService->sendNotifications($request);
    }
}
