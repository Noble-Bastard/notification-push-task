<?php

namespace App\NotificationsSender\Contracts;

use App\NotificationsSender\Data\NotificationInfoDTO;

interface NotificationsSendInterface
{
    public static function send(NotificationInfoDTO $notificationInfo, array $credentials);

}