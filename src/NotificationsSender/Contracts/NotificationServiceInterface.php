<?php

namespace App\NotificationsSender\Contracts;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for notification methods
 */
interface NotificationServiceInterface
{
    public function sendNotifications(Request $request);
}