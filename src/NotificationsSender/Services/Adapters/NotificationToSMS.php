<?php

namespace App\NotificationsSender\Services\Adapters;

use App\NotificationsSender\Contracts\NotificationsSendInterface;
use App\NotificationsSender\Data\NotificationInfoDTO;
use Exception;
use Twilio\TwiML\Voice\Client;

class NotificationToSMS implements NotificationsSendInterface
{

    /**
     * @param NotificationInfoDTO $notificationInfo
     * @param array $credentials
     * @return void
     * @throws Exception
     */
    public static function send(NotificationInfoDTO $notificationInfo, array $credentials): void
    {
        try {
            $client = new Client($credentials['sid'], $credentials['token']);

            $message = $client->messages->create(
                $notificationInfo->recipient,
                array(
                    'body' => $notificationInfo->body
                )
            );
        } catch (Exception $e) {
            throw new Exception($e->getMessage());

        }
    }
}