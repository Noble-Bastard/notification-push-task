<?php

namespace App\NotificationsSender\Services\Adapters;

use App\NotificationsSender\Contracts\NotificationsSendInterface;
use App\NotificationsSender\Data\NotificationInfoDTO;
use Exception;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class NotificationToAndroid implements NotificationsSendInterface
{

    /**
     * @param NotificationInfoDTO $notificationInfo
     * @param array $credentials
     * @return void
     * @throws Exception
     */
    public static function send(NotificationInfoDTO $notificationInfo, array $credentials): void
    {
        $factory = (new Factory)->withServiceAccount($credentials['account']);
        $messaging = $factory->createMessaging();

        $notification = Notification::create($notificationInfo->body);
        $message = CloudMessage::new()
            ->withNotification($notification);
        try {
            $messaging->send($message, $notificationInfo->recipient);
        } catch (MessagingException|FirebaseException $e) {
            throw new Exception($e->getMessage());
        }
    }
}