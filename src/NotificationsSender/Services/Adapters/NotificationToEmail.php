<?php

namespace App\NotificationsSender\Services\Adapters;

use App\NotificationsSender\Contracts\NotificationsSendInterface;
use App\NotificationsSender\Data\NotificationInfoDTO;
use Exception;

class NotificationToEmail implements NotificationsSendInterface
{

    /**
     * @param NotificationInfoDTO $notificationInfo
     * @param array $credentials
     * @return void
     * @throws Exception
     */
    public static function send(NotificationInfoDTO $notificationInfo, array $credentials = []): void
    {
        try {
            mail($notificationInfo->recipient, 'Notification', $notificationInfo->body);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());

        }
    }
}