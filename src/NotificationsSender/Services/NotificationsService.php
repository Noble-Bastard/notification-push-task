<?php

namespace App\NotificationsSender\Services;

use App\NotificationsSender\Contracts\NotificationServiceInterface;
use App\NotificationsSender\Data\NotificationInfoDTO;
use App\NotificationsSender\Services\Adapters\NotificationToEmail;
use App\NotificationsSender\Services\Adapters\NotificationToIOS;
use App\NotificationsSender\Services\Adapters\NotificationToSMS;
use Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service that sends Notifications
 */
class NotificationsService implements NotificationServiceInterface
{
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return void
     * @throws Exception
     * Желательно $credentials стоит вынести в конфиг, но по времени не успеваю сейчас)
     * Также желательно бы делать не статические вызовы, а по интерфейсу разделить для каждого отдельно
     */
    public function sendNotifications(Request $request): void
    {
        foreach ($request as $data) {
            if ($data['type'] == 'sms') {
                NotificationToSMS::send(
                    new NotificationInfoDTO($data['body'], $data['recipient']),
                    [
                        'sid' => 1111111,
                        'token' => 'ddd'
                    ]
                );
            }
            if ($data['type'] == 'ios') {
                NotificationToIOS::send(
                    new NotificationInfoDTO($data['body'], $data['recipient']),
                    [
                        'account' => 'account a',
                    ]
                );
            }
            if ($data['type'] == 'email') {
                NotificationToEmail::send(
                    new NotificationInfoDTO($data['body'], $data['recipient'])
                );
            }
            if ($data['type'] == 'android') {
                NotificationToIOS::send(
                    new NotificationInfoDTO($data['body'], $data['recipient']),
                    [
                        'account' => 'account a',
                    ]
                );
            }
        }
    }

}