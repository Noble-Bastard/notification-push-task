<?php

namespace App\NotificationsSender\Data;

class NotificationInfoDTO
{
    public function __construct(
        public readonly string $body,
        public readonly string $recipient
    ) {
    }
}